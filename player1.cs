using UnityEngine;

public class player1 : MonoBehaviour
{
    CharacterController characterController;

    public float playerSpeed = 10.0f;
    public float jumpSpeed = 5.0f;
    public float gravity = 25.0f;

    private Vector3 moveDirection;

    public GameObject cameraObj;
    private Vector3 cameraForward;

    //character position vector2
    public Vector2 characterPos;

    private readonly CharacterController controller;

    //character stats
    private float stamina = 1.0f;
    private float strength = 1.0f;
    private float speed = 1.0f;
    private float health = 100.0f;
    private float rotateSpeed = 10.0f;

    void setCharacterPos()
    {
        characterPos = new Vector2(transform.position.x, transform.position.z);
    }

    public Vector3 getCharacterPos()
    {
        return characterPos;
    }

    void getCamForward()
    {
        cameraForward = cameraObj.GetComponent<cameraScript>().forward;
    }

    void OnControllerColliderHit(ControllerColliderHit hit)
    {
        // Objects we touch, move them to position (0, 0, 0)
        hit.gameObject.transform.position = Vector3.zero;
    }

    void handleMovement()
    {
        if (characterController.isGrounded)
        {
            Vector3 walkingPos = new Vector3(cameraForward.x, 0.0f, cameraForward.z);
            transform.rotation = Quaternion.LookRotation(walkingPos);

            moveDirection.x = 0.0f;
            moveDirection.z = 0.0f;

            Vector2 forwardVect = new Vector2(cameraForward.x, cameraForward.z);
            Vector2 sideVect = Vector2.Perpendicular(forwardVect);

            if (Input.GetAxis("Horizontal") != 0)
            {
                moveDirection = new Vector3(Input.GetAxis("Horizontal") * -sideVect.x, 0.0f, Input.GetAxis("Horizontal") * -sideVect.y);
                moveDirection *= playerSpeed;
            }

            if (Input.GetAxis("Vertical") != 0)
            {
                moveDirection = new Vector3(Input.GetAxis("Vertical") * forwardVect.x, 0.0f, Input.GetAxis("Vertical") * forwardVect.y);
                moveDirection *= playerSpeed;

            }

            if (Input.GetButton("Jump"))
            {
                moveDirection.y = jumpSpeed;
            }


        }

        // Apply gravity. Gravity is multiplied by deltaTime twice (once here, and once below
        // when the moveDirection is multiplied by deltaTime). This is because gravity should be applied
        // as an acceleration (ms^-2)
        moveDirection.y -= gravity * Time.deltaTime;

        // Move the controller
        characterController.Move(moveDirection * Time.deltaTime);


    }

    void Start()
    {
        characterController = GetComponent<CharacterController>();

    }

    void Update()
    {
        setCharacterPos();
        getCamForward();
        //check if character controller is present
        characterController = GetComponent<CharacterController>();
        if (!characterController) //Check for controller reference
        {
            Debug.LogError("Didn't find CharacterController");
        }
        else
        {
            handleMovement();

        }
    }
}