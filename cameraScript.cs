﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraScript : MonoBehaviour
{
    public float rotY = 0;
    public Transform target;
    public float speed = 60;
    public Vector3 forward;
    public Vector3 camPos;
    public GameObject characterObj;

    void getCharacterPos()
    {
        camPos = characterObj.GetComponent<player1>().getCharacterPos();
        //Debug.Log("CAM CHAR POS: " + camPos);
    }

    void setForward()
    {
        forward = transform.forward;
    }

    void detect()
    {
        transform.LookAt(target.position);

        if (Input.GetAxis("Mouse X") < 0)
        {
            //Code for action on mouse moving left
            transform.RotateAround(target.position, target.up, speed * Time.deltaTime);
        }
        if (Input.GetAxis("Mouse X") > 0)
        {
            //Code for action on mouse moving right
            transform.RotateAround(target.position, target.up, -speed * Time.deltaTime);
        }
        if (Input.GetAxis("Mouse Y") > 0)
        {
            //Code for action on mouse moving right
            transform.RotateAround(target.position, target.right, speed * Time.deltaTime);
        }
        if (Input.GetAxis("Mouse Y") < 0)
        {
            //Code for action on mouse moving right
            transform.RotateAround(target.position, target.right, -speed * Time.deltaTime);
        }
    }

    void Start()
    {
    }
    void Update()
    {
        getCharacterPos();
        setForward();
        detect();
    }




}
